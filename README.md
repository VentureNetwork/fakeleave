# FakeLeave

Basic Spigot plugin that displays a fake-leave message.

## Getting Started

Very simply, type `/fl` into the chat. Operator permissions are required.

### Prerequisites

Essentials OR EssentialsX

### Installing

Download the plugin from the `out` folder. Titled `FakeLeave.jar`

Place the plugin into `plugins/` in the root of your server directory.

Continue in *Deployment* below.

## Deployment

Launch your server using the `start.bat`, `start.sh`, or `java -jar Spigot.jar`

## Authors

* **Jared Kozel** - *Entire development* - [Jared Kozel](https://gitlab.com/DynamicDonkey)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Spigot Minecraft Server Software
* VentureNetwork

