package us.venturenetwork.fakeleave;


import org.bukkit.command.CommandExecutor;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.command.CommandSender;

public class CommandFakeLeaveHelp implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {


            Player player = (Player) sender;
            player.sendMessage("§9§lVenture§f§lNetwork §aFakeLeave");
            player.sendMessage("§m--------------------------------------");
            player.sendMessage("§a§o/flhelp - §rShows help dialog");
            player.sendMessage("§a§o/fl - §rDisplays the \"leave\" text");
            player.sendMessage("§a§o/fj - §rDisplays the \"join\" text");

        }

        return true;
    }

}