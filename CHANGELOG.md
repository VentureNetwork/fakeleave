# Changelog

## [1.0.0] - 2018-06-20
### Added
- First version
- Basic codebase, will be expanded upon.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).